/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.io.tiff;

import edu.ucr.cs.bdlab.beast.util.MathUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * A tile from a TIFF file that might not be decompressed. The tile is decompressed lazily when the first pixel value
 * is requested. This allows the tile to be efficiently kept in memory and serialized without decompressing it.
 * This class is designed to
 * be standalone and is not associated with an open TIFF file. This makes it possible to serialize this class over
 * network and read the pixel values on another machine.
 */
public class DiskTiffTile extends AbstractTiffTile {

  /**The path of the TIFF file*/
  private final String path;

  /**The offset in the file to start reading the tile*/
  private final long offset;

  /**The number of bytes to read from disk for this tile*/
  private final int length;

  /**The decompressed tile data. Only set after the tile is loaded from disk.*/
  transient private byte[] tileData;

  /**The compression scheme of the data*/
  private int compressionScheme;

  /**The predictor of the pixel values, e.g., diff*/
  private int predictor;

  public DiskTiffTile(String path, long offset, int length,
                      int compressionScheme, int predictor,
                      int[] bitsPerSample, int[] sampleFormats, int bitsPerPixel,
                      int i1, int j1, int i2, int j2, int planarConfiguration, boolean littleEndian) {
    super(i1, j1, i2, j2, bitsPerSample, sampleFormats, bitsPerPixel, planarConfiguration, littleEndian);
    this.path = path;
    this.offset = offset;
    this.length = length;
    this.compressionScheme = compressionScheme;
    this.predictor = predictor;
  }

  /**Cached default Hadoop configuration to create FileSystem and avoid the costly loading of Configuration*/
  static final Configuration defaultHadoopConf = new Configuration();

  /**
   * Return the decompressed array of bytes with the tile data. If the tile is not loaded, it lazily loads it.
   * The loaded data is not serialized to keep the serialization cost low.
   * @return an array of decompressed tile data
   */
  public byte[] getTileData() {
    if (this.tileData == null) {
      // First time. Read data from disk and decompress it if needed
      this.tileData = new byte[length];
      Path p = new Path(path);
      try {
        FileSystem fs = p.getFileSystem(defaultHadoopConf);
        try (FSDataInputStream in = fs.open(p)) {
          in.seek(offset);
          in.readFully(this.tileData);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      if (compressionScheme != TiffConstants.COMPRESSION_NONE) {
        // Need to decompress the data first
        switch (compressionScheme) {
          case TiffConstants.COMPRESSION_LZW:
            tileData = LZWCodec.decode(tileData,
                getTileWidth() * getTileHeight() * bitsPerPixel / 8, true);
            break;
          case TiffConstants.COMPRESSION_DEFLATE:
            try {
              Inflater inflater = new Inflater();
              inflater.setInput(tileData);
              tileData = new byte[getTileWidth() * getTileHeight() * bitsPerPixel / 8];
              int decompressionLength = inflater.inflate(tileData);
              assert decompressionLength == tileData.length :
                  String.format("Mismatching length between. Decompressed length %d != expected length %d",
                      decompressionLength, tileData.length);
            } catch (DataFormatException e) {
              throw new RuntimeException("Error inflating TIFF tile", e);
            }
            break;
          default:
            throw new RuntimeException(String.format("Unsupported compression scheme %d", compressionScheme));
        }
        compressionScheme = TiffConstants.COMPRESSION_NONE;
      }
      if (predictor == 2) {
        // Apply the differencing algorithm
        // Special case when all components are 8-bits which make the differencing simpler
        int minBitsPerSample = bitsPerSample[0];
        int maxBitsPerSample = bitsPerSample[0];
        for (int iSample = 1; iSample < getNumSamples(); iSample++) {
          minBitsPerSample = Math.min(minBitsPerSample, bitsPerSample[iSample]);
          maxBitsPerSample = Math.max(maxBitsPerSample, bitsPerSample[iSample]);
        }
        if (minBitsPerSample == 8 && maxBitsPerSample == 8) {
          // This is the easiest case to handle with an efficient algorithm
          for (int jPixel = 0; jPixel < getTileHeight(); jPixel++) {
            int offset = (jPixel * getTileWidth() + 1) * getNumSamples();
            int endOffset = ((jPixel + 1) * getTileWidth()) * getNumSamples();
            while (offset < endOffset) {
              tileData[offset] += tileData[offset - getNumSamples()];
              offset++;
            }
          }
        } else if (minBitsPerSample == 16 && maxBitsPerSample == 16) {
          // Values are short integers
          int numSamples = getNumSamples();
          short[] previousPixel = new short[numSamples];
          for (int jPixel = 0; jPixel < getTileHeight(); jPixel++) {
            int offset = (jPixel * getTileWidth()) * numSamples * 2;
            int endOffset = ((jPixel + 1) * getTileWidth()) * numSamples * 2;
            for (int iSample = 0; iSample < numSamples; iSample++) {
              previousPixel[iSample] = (short) ((tileData[offset] & 0xff) | ((tileData[offset + 1] & 0xff) << 8));
              offset += 2;
            }
            while (offset < endOffset) {
              for (int iSample = 0; iSample < numSamples; iSample++) {
                short diff = (short) ((tileData[offset] & 0xff) | ((tileData[offset + 1] & 0xff) << 8));
                previousPixel[iSample] += diff;
                tileData[offset] = (byte) previousPixel[iSample];
                tileData[offset + 1] = (byte) (previousPixel[iSample] >> 8);
                offset += 2;
              }
            }
          }
        } else {
          if (planarConfiguration == TiffConstants.PlanarFormat)
            throw new RuntimeException("Does not yet support PlanarFormat");
          // General case could be less efficient
          int numSamples = getNumSamples();
          int[] previousPixel = new int[numSamples];
          for (int jPixel = 0; jPixel < getTileHeight(); jPixel++) {
            // Offset is in bits
            int offset = (jPixel * getTileWidth()) * bitsPerPixel;
            int endOffset = ((jPixel + 1) * getTileWidth()) * bitsPerPixel;
            // Read first pixel (reference pixel)
            for (int iSample = 0; iSample < numSamples; iSample++) {
              previousPixel[iSample] = (int) MathUtil.getBits(tileData, offset, bitsPerSample[iSample]);
              offset += bitsPerSample[iSample];
            }
            while (offset < endOffset) {
              for (int iSample = 0; iSample < numSamples; iSample++) {
                int diffValue = (int) MathUtil.getBits(tileData, offset, bitsPerSample[iSample]);
                int correctValue = previousPixel[iSample] + diffValue;
                MathUtil.setBits(tileData, offset, bitsPerSample[iSample], correctValue);
                previousPixel[iSample] = correctValue;
                offset += bitsPerSample[iSample];
              }
            }
          }
        }
        predictor = 0;
      }
    }
    return tileData;
  }
}
