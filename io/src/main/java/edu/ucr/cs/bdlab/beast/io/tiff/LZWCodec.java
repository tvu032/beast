/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.io.tiff;

import edu.ucr.cs.bdlab.beast.util.BitInputStream;
import edu.ucr.cs.bdlab.beast.util.BitOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Decodes LZW-compressed data from a TIFF file.
 * This code is based on the description in the TIFF file specs.
 */
public class LZWCodec {

  /**A code to clear the message table*/
  public static final int ClearCode = 256;

  /**The code that signals the end of information*/
  public static final int EOI_CODE = 257;

  /**The index of the array that contains the *offset* in the decode table array*/
  private static final int Offset = 0;
  /**The index of the array that contains the *length* in the decode table array*/
  private static final int Length = 1;

  static class DecodedMessage {
    public byte[] data;
    public int length;

    public DecodedMessage(int initialCapacity) {
      this.data = new byte[initialCapacity];
    }

    private void ensureCapacity(int newLength) {
      if (newLength > data.length)
        data = Arrays.copyOf(data, newLength);
    }

    public void append(byte[] data, int offset, int dataLength) {
      ensureCapacity(this.length + dataLength);
      System.arraycopy(data, offset, this.data, this.length, dataLength);
      this.length += dataLength;
    }

    public void append(byte value) {
      ensureCapacity(this.length + 1);
      data[this.length++] = value;
    }
  }

  /**
   * Retrieve a value from the table and write it to the given output stream.
   * @param decodedMessage the output stream to write the decoded message to.
   * @param decodeTable the decode table
   * @param code the code to retrieve from the table.
   * @throws IOException
   */
  static void getAndWriteValue(DecodedMessage decodedMessage, int[][] decodeTable, int code) throws IOException {
    if (code <= 256)
      decodedMessage.append((byte) code);
    else
      decodedMessage.append(decodedMessage.data, decodeTable[Offset][code], decodeTable[Length][code]);
  }

  /**
   * Decodes LZW-encoded message. See TIFF specs page 61 for details.
   * @param encodedData the encoded data as an array of bytes
   * @param expectedDecodeSize (optional) A hint about the decoded message size to reduce dynamic memory allocation.
   * @param terminateAtSize terminate when the size of the decoded message reaches the expected decode size.
   *                        This flag is useful when the encoded message might not have a proper terminator.
   * @return the decoded data as an array of bytes
   */
  public static byte[] decode(byte[] encodedData, int expectedDecodeSize, boolean terminateAtSize) {
    int terminatingSize = terminateAtSize? expectedDecodeSize : Integer.MAX_VALUE;
    BitInputStream inputBits = new BitInputStream(encodedData);
    if (expectedDecodeSize == 0)
      expectedDecodeSize = encodedData.length * 5;
    DecodedMessage decodedMessage = new DecodedMessage(expectedDecodeSize);
    int[][] decodeTable = new int[2][4096];
    int decodeTableSize = 0;
    short code, oldCode = -1;
    int length = 9;
    int remainingBits = encodedData.length * 8;
    int offsetOldCode = 0;
    code = inputBits.nextShort(length);
    try {
      while (code != EOI_CODE && length < remainingBits && decodedMessage.length < terminatingSize) {
        remainingBits -= length;
        if (code == ClearCode) {
          // Clear the table
          // +2 is for Clear Code and EOI special codes
          decodeTableSize = 256 + 2;
          length = 9;
          code = inputBits.nextShort(length);
          remainingBits -= length;
          if (code == EOI_CODE)
            break;
          offsetOldCode = decodedMessage.length;
          getAndWriteValue(decodedMessage, decodeTable, code);
          oldCode = code;
        } /* end of ClearCode case */ else {
          int currentOffset = decodedMessage.length;
          if (code < decodeTableSize) {
            // code in table
            getAndWriteValue(decodedMessage, decodeTable, code);
          } else {
            assert code == decodeTableSize : String.format("Unexpected new code %d while table size is %d", code, decodeTableSize);
            getAndWriteValue(decodedMessage, decodeTable, oldCode);
            decodedMessage.append(decodedMessage.data[currentOffset]);
          }
          addStringToTable(decodeTable, decodeTableSize, offsetOldCode, currentOffset - offsetOldCode + 1);
          decodeTableSize++;
          offsetOldCode = currentOffset;
          oldCode = code;
          if (decodeTableSize - 1 == 510)
            length = 10;
          else if (decodeTableSize - 1 == 1022)
            length = 11;
          else if (decodeTableSize - 1 == 2046)
            length = 12;
        }
        code = inputBits.nextShort(Math.min(length, remainingBits));
      }
    } catch (IOException e) {
      throw new RuntimeException("Error decoding LZW", e);
    }

    if (decodedMessage.length != decodedMessage.data.length)
      decodedMessage.data = Arrays.copyOf(decodedMessage.data, decodedMessage.length);
    return decodedMessage.data;
  }

  private static void addStringToTable(int[][] decodeTable, int size, int offset, int length) {
    decodeTable[Offset][size] = offset;
    decodeTable[Length][size] = length;
  }


  static class EncodeTable {
    /**Table size including the implicit characters (0-255) and control codes*/
    int tableSize;

    Map<Integer, Integer> encoded = new HashMap<>();

    public EncodeTable() {
      this.clear();
    }

    public void clear() {
      encoded.clear();
      tableSize = 258;
    }

    public int size() {
      return tableSize;
    }

    /**
     * Checks if the message that consists of an existing code (prev) concatenated with a new character (nextChar)
     * exists in the table. prev=-1 indicates no previous message which makes the result always true.
     * @param prev code of a previous message or -1 if the previous message is null
     * @param nextChar the character to append to the previous message
     * @return {@code true} if the the previous message + nextChar exists in the table
     */
    public boolean exists(int prev, int nextChar) {
      if (prev == -1)
        return true;
      return encoded.containsKey((prev << 8) | nextChar);
    }

    public int codeFromString(int prev, int nextChar) {
      if (prev == -1)
        return nextChar & 0xff;
      assert(exists(prev, nextChar));
      return encoded.get((prev << 8) | nextChar);
    }

    public void addTableEntry(int prev, int nextChar) {
      assert(!exists(prev, nextChar));
      encoded.put((prev << 8) | nextChar, tableSize++);
    }
  }

  public static byte[] encode(byte[] message) {
    try {
      ByteArrayOutputStream encodedMessage = new ByteArrayOutputStream();
      BitOutputStream encodeOutput = new BitOutputStream(encodedMessage);
      int prev = -1;
      int nBits = 9;
      encodeOutput.write(ClearCode, nBits);
      EncodeTable encodeTable = new EncodeTable();
      for (int i = 0; i < message.length; i++) {
        int nextChar = message[i] & 0xff;
        if (encodeTable.exists(prev, nextChar)) {
          // String already exists, append and move on
          prev = encodeTable.codeFromString(prev, nextChar);
        } else {
          // New string. Write previous and add the string to table for future occurrences
          encodeOutput.write(prev, nBits);
          encodeTable.addTableEntry(prev, nextChar);
          if (encodeTable.size() == 512)
            nBits++;
          else if (encodeTable.size() == 1024)
            nBits++;
          else if (encodeTable.size() == 2048)
            nBits++;
          else if (encodeTable.size() == 4095) {
            // Clear the table and write clear code
            encodeOutput.write(ClearCode, nBits);
            encodeTable.clear();
            nBits = 9;
          }
          prev = nextChar;
        }
      }
      // Write last character
      encodeOutput.write(prev, nBits);
      if (prev < 256) {
        // When decoding, the decoder will add a new entry not knowing that this is the last character
        // This will result in incorrectly decoding the EOI_CODE unless we adjust its length accordingly
        int decodeTableSize = encodeTable.size() + 1;
        // Increase table size for the last EOI code
        if (decodeTableSize == 512)
          nBits++;
        else if (decodeTableSize == 1024)
          nBits++;
        else if (decodeTableSize == 2048)
          nBits++;
      }
      encodeOutput.write(EOI_CODE, nBits);
      encodeOutput.close();
      return encodedMessage.toByteArray();
    } catch (IOException e) {
      throw new RuntimeException("Unexpected error while encoding", e);
    }
  }
}
