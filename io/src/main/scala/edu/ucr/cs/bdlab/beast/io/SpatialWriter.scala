/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.io

import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes._
import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{EnvelopeND, IFeature}
import edu.ucr.cs.bdlab.beast.indexing.IndexHelper.getPartitionAsText
import edu.ucr.cs.bdlab.beast.synopses.Summary
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path, PathFilter}
import org.apache.spark.TaskContext
import org.apache.spark.internal.Logging
import org.apache.spark.util.TaskFailureListener

/**
  * A helper class for writing the output
  */
object SpatialWriter extends Logging {


  /**Java shortcut*/
  def saveFeaturesJ(features: JavaSpatialRDD,  oFormat: String, outPath: String, opts: BeastOptions): Unit =
    saveFeatures(features.rdd, oFormat, outPath, opts)

  /**
    * Saves the given set of features to the output using the provided output format.
    * @param features the set of features to store to the output
    * @param oFormat the output format to use for writing
    * @param outPath the path to write the output to
    * @param opts user options to configure the writer
    */
  def saveFeatures(features: SpatialRDD,  oFormat: String, outPath: String, opts: BeastOptions): Unit = {
    val hadoopConf = opts.loadIntoHadoopConf(new Configuration(features.sparkContext.hadoopConfiguration))
    SpatialOutputFormat.setOutputFormat(hadoopConf, oFormat)
    // Delete existing output if overwrite flag is set
    val out: Path = new Path(outPath)
    val filesystem: FileSystem = out.getFileSystem(hadoopConf)
    if (opts.getBoolean(SpatialOutputFormat.OverwriteOutput, false)) {
      if (filesystem.exists(out))
        filesystem.delete(out, true)
    }
    // Extract the spatial writer class
    val writerClass: Class[_ <: FeatureWriter] = SpatialOutputFormat.getConfiguredFeatureWriterClass(hadoopConf)
    val files: Array[String] = features.sparkContext.runJob(features, (context: TaskContext, fs: Iterator[IFeature]) => {
      // Get writer metadata to determine the extension of the output file
      val metadata: FeatureWriter.Metadata = writerClass.getAnnotation(classOf[FeatureWriter.Metadata])
      // Create a temporary directory for this task output
      val tempDir: Path = new Path(new Path(outPath), f"temp-${context.taskAttemptId()}")
      val fileSystem = tempDir.getFileSystem(opts.loadIntoHadoopConf())
      context.addTaskFailureListener(new TaskFailureListener() {
        override def onTaskFailure(context: TaskContext, error: Throwable): Unit = {
          if (fileSystem.exists(tempDir))
            fileSystem.delete(tempDir, true)
        }
      })
      fileSystem.mkdirs(tempDir)
      val partitionId: Int = context.partitionId()
      // Initialize the feature writer
      val partitionFileName: String = f"part-${partitionId}%05d${metadata.extension()}"
      val partitionFile: Path = new Path(tempDir, partitionFileName)
      val featureWriter = writerClass.newInstance()
      featureWriter.initialize(partitionFile, opts.loadIntoHadoopConf())
      for (feature <- fs)
        featureWriter.write(feature)
      featureWriter.close()
      partitionFile.toString
    })
    for (filename <- files.filter(_ != null)) {
      val partitionPath = new Path(filename)
      // Move the file to the output directory
      filesystem.rename(partitionPath, new Path(out, partitionPath.getName))
    }
    // Clean up temporary output directory
    filesystem.listStatus(out, new PathFilter {
      override def accept(path: Path): Boolean = path.getName.startsWith("temp-")
    }).foreach(f => filesystem.delete(f.getPath, true))
  }
}
