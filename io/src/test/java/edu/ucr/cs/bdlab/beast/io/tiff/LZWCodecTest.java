package edu.ucr.cs.bdlab.beast.io.tiff;

import edu.ucr.cs.bdlab.test.JavaSparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class LZWCodecTest extends JavaSparkTest {

  public void testSimpleEncodeDecode() {
    byte[] message = {7, 7, 7, 8, 8, 7, 7, 6, 6};
    byte[] encodedMessage = LZWCodec.encode(message);
    byte[] decodedMessage = LZWCodec.decode(encodedMessage, 0, false);
    assertArrayEquals(message, decodedMessage);
  }

  public void testRandomMessages() {
    if (shouldRunStressTest()) {
      for (int m = 0; m < 100; m++) {
        Random r = new Random(m);
        int messageLength = r.nextInt(1000) + 20;
        byte[] message = new byte[messageLength];
        for (int i = 0; i < message.length; i++)
          message[i] = (byte) r.nextInt();
        byte[] encodedMessage = LZWCodec.encode(message);
        byte[] decodedMessage = LZWCodec.decode(encodedMessage, 0, false);
        assertArrayEquals(message, decodedMessage);
      }
    }
  }
}