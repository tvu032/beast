/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.sql

import edu.ucr.cs.bdlab.beast.geolite.GeometryReader
import org.apache.spark.beast.sql.GeometryDataType
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.analysis.TypeCheckResult
import org.apache.spark.sql.catalyst.expressions.codegen.CodegenFallback
import org.apache.spark.sql.catalyst.expressions.{Expression, UnsafeArrayData}
import org.apache.spark.sql.types.DataType
import org.locationtech.jts.geom.{Coordinate, Geometry, GeometryFactory}

/**
 * Takes a list of point locations and IDs and creates either a linestring or
 * polygon based on whether the last point is the same as the first point or not
 *
 * @param inputExpressions
 */
case class ST_CreateLinePolygon(inputExpressions: Seq[Expression]) extends Expression with CodegenFallback {
  /** Geometry factory to create geometries */
  val geometryFactory: GeometryFactory = GeometryReader.DefaultGeometryFactory

  val inputArity: Int = 2

  override def checkInputDataTypes(): TypeCheckResult = {
    if (inputArity == -1 || inputExpressions.length == inputArity)
      TypeCheckResult.TypeCheckSuccess
    else
      TypeCheckResult.TypeCheckFailure(s"Function $prettyName expects $inputArity inputs but received ${inputExpressions.length}")
  }

  override def children: Seq[Expression] = inputExpressions

  override def nullable: Boolean = false

  override def foldable: Boolean = inputExpressions.forall(_.foldable)

  override def dataType: DataType = GeometryDataType

  override def eval(input: InternalRow): Any = {
    val inputValues: Seq[Any] = inputExpressions.map(e => e.eval(input))
    val input1 = inputValues.head.asInstanceOf[UnsafeArrayData]
    val input2 = inputValues(1).asInstanceOf[UnsafeArrayData]
    val result: Geometry = performFunction(input1, input2)
    GeometryDataType.setGeometryInRow(result)
  }

  def performFunction(pointIDs: UnsafeArrayData, coordinates: UnsafeArrayData): Geometry = {
    val pointIdsArray = pointIDs.toLongArray
    val coordinatesArray = coordinates.toDoubleArray
    val is_polygon = pointIdsArray(0) == pointIdsArray(pointIdsArray.length - 1)
    val coords = new Array[Coordinate](coordinatesArray.length / 2)
    for (i <- coordinatesArray.indices by 2) {
      if (i == coordinatesArray.length - 2 && is_polygon) {
        coords(i / 2) = coords(0)
      } else {
        coords(i / 2) = new Coordinate(coordinatesArray(i), coordinatesArray(i + 1))
      }
    }

    if (is_polygon) {
      if (coords.length == 1 || coords.length == 2) {
        geometryFactory.createPoint(coords(0))
      } else if (coords.length == 3) {
        geometryFactory.createLineString(coords.take(2))
      } else {
        geometryFactory.createPolygon(coords)
      }
    } else {
      geometryFactory.createLineString(coords)
    }
  }
}