/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.geolite

import java.awt.geom.Point2D
import scala.reflect.ClassTag

/**
 * An interface that represents a tile
 */
trait ITile extends Serializable {
  /** The ID of this tile within its raster layer */
  def tileID: Int

  /** The lowest index of the column of this tile (inclusive)*/
  def x1: Int = rasterMetadata.getTileX1(tileID)

  /**The highest index of the column of this tile (inclusive)*/
  def x2: Int = rasterMetadata.getTileX2(tileID)

  /**Number of columns in the tile*/
  def tileWidth: Int = x2 - x1 + 1

  /** The lowest index of a row of this tile (inclusive) */
  def y1: Int = rasterMetadata.getTileY1(tileID)

  /** The highest index of a row of this tile (inclusive)*/
  def y2: Int = rasterMetadata.getTileY2(tileID)

  /**Total number of scan lines in this tile*/
  def tileHeight: Int = y2 - y1 + 1

  /**The metadata of the raster file*/
  def rasterMetadata: RasterMetadata

  /**
   * The value of the given pixel which depends on the tile data type and number of bands.
   * For example, if the data type is integer with three bands, it will return an integer array of 3.
   * If the data type is float and one band, it will return a single float value.
   * @param i the index of the column
   * @param j the index of the row
   * @return the pixel value
   */
  def getPixelValue(i: Int, j: Int): Any

  /**
   * Converts a primitive value to the given data type using numeric value conversion.
   * An integer is converted to a float but a float is not converted to integer.
   * If the desired type is integer and the raw value is float, an exception is thrown
   * @param rawVal the original value
   * @param t the class of the desired type
   * @tparam T the data type to convert to
   * @return the value after converting to the desired type.
   */
  private def getAs[T](rawVal: Any)(implicit t: ClassTag[T]): T = {
    if (rawVal.isInstanceOf[T])
      rawVal.asInstanceOf[T]
    else {
      val rawType: Class[_] = rawVal.getClass
      val desiredType: Class[_] = t.runtimeClass
      if (rawType == classOf[Int] && desiredType == classOf[Float])
        rawType.asInstanceOf[Int].toFloat.asInstanceOf[T]
      else if (rawType == classOf[Integer] && desiredType == classOf[Float])
        rawType.asInstanceOf[Integer].toFloat.asInstanceOf[T]
      else
        throw new RuntimeException(s"Cannot convert '${rawVal}' with type ${rawType} to ${desiredType}")
    }
  }

  def getPointValueAs[T](x: Double, y: Double)(implicit t: ClassTag[T]): T = {
    val pixel: Point2D.Double = new Point2D.Double()
    rasterMetadata.modelToGrid(x, y, pixel)
    getPixelValueAs[T](pixel.x.toInt, pixel.y.toInt)(t)
  }

  /**
   * Return the value of the given pixel and convert it to integer. This function will only work if each pixel
   * contains only one component. If the raw value is float, this function will fail.
   * @param i the index of the column
   * @param j the index of the row
   * @return the value of the pixel after converting it to the given data type.
   */
  def getPixelValueAsInt(i: Int, j: Int): Int = {
    require(numComponents == 1,
      s"This function only works for tiles with one component but this tile has ${numComponents}")
    getPixelValueAs[Int](i, j)
  }

  /**
   * Return the value of the pixel that contains the given point at model (world) coordinates
   * @param x the x-coordinate of the point, e.g., longitude
   * @param y the y-coordinate of the point, e.g., latitude
   * @return the value of all components of the given pixel concatenated in one integer
   */
  def getPointValueAsInt(x: Double, y: Double): Int = getPointValueAs[Int](x, y)

  /**
   * Return the values of all components of the pixel that contains the given point as an array of integers
   * @param x the x-coordinate of the point, e.g., longitude
   * @param y the y-coordinate of the point, e.g., latitude
   * @return the value of all components of the given pixel in an array
   */
  def getPointValuesAsInt(x: Double, y: Double): Array[Int] = getPointValueAs[Array[Int]](x, y)

  /**
   * Return the value of the given pixel as an array of integer. If the pixel contains only one component,
   * an array of size 1 is returned. If the pixel value (or any of its components) is float,
   * an exception is thrown.
   * @param i the index of the column
   * @param j the index of the row
   * @return the value of the pixel after converted to an array of integer
   */
  def getPixelValuesAsInt(i: Int, j: Int): Array[Int] = getPixelValueAs[Array[Int]](i, j)

  /**
   * Return the value of the given pixel and convert it to float. This function will only work if each pixel
   * contains only one component. If the raw value is integer, it will be typecast to float.
   * @param i the index of the column
   * @param j the index of the row
   * @return the value of the pixel after converting it to the given data type.
   */
  def getPixelValueAsFloat(i: Int, j: Int): Float = {
    require(numComponents == 1,
      s"This function only works for tiles with one component but this tile has ${numComponents}")
    getPixelValueAs[Float](i, j)
  }

  /**
   * Return the value of the given pixel as an array of float. If the pixel contains only one component,
   * an array of size 1 is returned. If the pixel value (or any of its components) is float,
   * an exception is thrown.
   * @param i the index of the column
   * @param j the index of the row
   * @return the value of the pixel after converted to an array of float
   */
  def getPixelValuesAsFloat(i: Int, j: Int): Array[Float] = getPixelValueAs[Array[Float]](i, j)

  /**
   * Return the value of the pixel at the given point as float. If it is integer, it will be typecast to float.
   * @param x the x-coordinate of the point
   * @param y the y-coordinate of the point
   * @return the point value as float
   */
  def getPointValuesAsFloat(x: Double, y: Double): Array[Float] = getPointValueAs[Array[Float]](x, y)

  /**
   * The value of the pixel located at the given model (world) location, e.g., longitude and latitude
   * @param x the x-coordinate of the point
   * @param y the y-coordinate of the point
   * @return the value of the pixel at the given point location
   */
  def getPointValue(x: Double, y: Double): Any = {
    val pixel: Point2D.Double = new Point2D.Double()
    rasterMetadata.modelToGrid(x, y, pixel)
    getPixelValue(pixel.x.toInt, pixel.y.toInt)
  }

  /**
   * Return the value of the given pixel after converting it to the given type.
   * The following conversions are done automatically:
   *  - If the pixel value is integer and the desired type is float, typecast is used
   * @param i the index of the column
   * @param j the index of the row
   * @tparam T the desired data type of the output
   * @return the value of the pixel after converting it to the given data type.
   */
  def getPixelValueAs[T](i: Int, j: Int)(implicit t: ClassTag[T]): T = {
    val rawValue = getPixelValue(i, j)
    val desiredClass: Class[_] = t.runtimeClass
    val actualClass: Class[_] = rawValue.getClass
    // Check if any type conversion is needed
    // While the first condition is similar to the last, it would be faster in the basic case
    if (desiredClass == actualClass)
      rawValue.asInstanceOf[T]
    else if (desiredClass == classOf[Float] && actualClass == classOf[Int])
      rawValue.asInstanceOf[Int].toFloat.asInstanceOf[T]
    else if (desiredClass == classOf[Float] && actualClass == classOf[java.lang.Integer])
      rawValue.asInstanceOf[Integer].toFloat.asInstanceOf[T]
    else if (desiredClass.isArray && !actualClass.isArray) {
      val cType = desiredClass.getComponentType
      val retValue = java.lang.reflect.Array.newInstance(cType, numComponents)
      if (numComponents == 1) {
        java.lang.reflect.Array.set(retValue, 0, getAs(rawValue)(ClassTag(cType)))
      } else {
        val rawValueArray: Array[AnyVal] = rawValue.asInstanceOf[Array[AnyVal]]
        for (b <- 0 until numComponents)
          java.lang.reflect.Array.set(retValue, b, getAs(rawValueArray(b))(ClassTag(cType)))
      }
      retValue.asInstanceOf[T]
    }
    else
      rawValue.asInstanceOf[T]
  }

  /**
   * Check whether the given pixel is empty or not. A pixel is empty if it has no data or if its pixel value is
   * equal to a fill value defined by the raster layer.
   * @param i the index of the column
   * @param j the index of the row
   * @return `true` if pixel has a valid value or `false` if it does not.
   */
  def isEmpty(i: Int, j: Int): Boolean

  /**
   * Number of components for each pixel in this raster file.
   * @return the number of components in each pixel, e.g., 3 for RGB or 1 for grayscale
   */
  def numComponents: Int

  override def toString: String =
    s"Tile #$tileID ($x1,$y1) - ($x2,$y2)"
}
