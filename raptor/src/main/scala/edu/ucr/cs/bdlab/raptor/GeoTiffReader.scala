/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.io.SpatialReaderMetadata
import edu.ucr.cs.bdlab.beast.io.tiff.{ITiffReader, TiffConstants, TiffRaster}
import edu.ucr.cs.bdlab.beast.util.{BitArray, IConfigurable, OperationParam}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkConf
import org.apache.spark.beast.CRSServer
import org.apache.spark.internal.Logging
import org.opengis.referencing.crs.CoordinateReferenceSystem

import java.awt.geom.AffineTransform
import java.io.{ByteArrayInputStream, DataInputStream}
import java.nio.ByteBuffer

/**
 * A reader of GeoTIFF files.
 */
@SpatialReaderMetadata(
  description = "Opens GeoTIFF files",
  extension = ".tif",
  shortName = "geotiff",
  filter = "*.tif\n*.geotiff\n*.tiff",
)
class GeoTiffReader extends IRasterReader with Logging {
  import GeoTiffReader._
  import GeoTiffConstants._

  private var tiffReader: ITiffReader = _

  private var tiffRaster: TiffRaster = _

  private var fillValue: Int = 0

  /**A list of valid tiles if defined in the GeoTiff file*/
  private var validTiles: BitArray = new BitArray()

  /**The metadata of this file*/
  private var rasterMetadata: RasterMetadata = _

  /**Instruct the reader to return lazy tiles*/
  private var loadTilesLazily: Boolean = false

  override def initialize(fileSystem: FileSystem, path: String, layer: String,
                          opts: BeastOptions, sparkConf: SparkConf): Unit = {
    super.initialize(fileSystem, path, layer, opts, sparkConf)
    val iLayer = layer.toInt
    this.loadTilesLazily = opts.getBoolean(LazyTiles, false)
    this.initialize(fileSystem, new Path(path), iLayer)
  }

  private def initialize(fileSystem: FileSystem, path: Path, iLayer: Int): Unit = {
    // Loads the GeoTIFF file from the given path
    tiffReader = ITiffReader.openFile(fileSystem, path)
    tiffRaster = tiffReader.getLayer(iLayer)
    //raster.dumpEntries(System.out);
    // Define grid to model transformation (G2M)
    var buffer: ByteBuffer = null
    val g2m: AffineTransform = new AffineTransform
    var entry = tiffRaster.getEntry(ModelTiepointTag)
    if (entry != null) { // Translate point
      buffer = tiffReader.readEntry(entry, buffer)
      val dx = buffer.getDouble(3 * 8) - buffer.getDouble(0 * 8)
      val dy = buffer.getDouble(4 * 8) - buffer.getDouble(1 * 8)
      g2m.translate(dx, dy)
    }
    entry = tiffRaster.getEntry(ModelPixelScaleTag)
    if (entry != null) { // Scale point
      buffer = tiffReader.readEntry(entry, null)
      val sx = buffer.getDouble(0 * 8)
      val sy = buffer.getDouble(1 * 8)
      g2m.scale(sx, -sy)
    }

    entry = tiffRaster.getEntry(GDALNoDataTag)

    if (entry != null) {
      buffer = tiffReader.readEntry(entry, null)
      val prefix = Character.getNumericValue(buffer.get(0))
      if (prefix != -1) fillValue = prefix
      else fillValue = 0
      for (i <- 1 until buffer.capacity - 1) {
        val value = buffer.get(i)
        val temp = Character.getNumericValue(value)
        fillValue = fillValue * 10 + temp
      }
      if (prefix == -1) fillValue *= -1
    } else fillValue = Integer.MIN_VALUE

    entry = tiffRaster.getEntry(ValidTilesBitMask)
    if (entry != null) {
      validTiles = new BitArray(tiffRaster.getNumTilesX * tiffRaster.getNumTilesY)
      val validTilesData = ByteBuffer.allocate((tiffRaster.getNumTilesX * tiffRaster.getNumTilesY + 7) / 8)
      tiffReader.readEntry(entry, validTilesData)
      val din = new DataInputStream(new ByteArrayInputStream(validTilesData.array()))
      validTiles.readBitsMinimal(din)
    } else {
      validTiles = null
    }

    // call to class to get metadata.
    val metadata = new GeoTiffMetadata(tiffReader, tiffRaster)
    val gtcs = new GeoTiffMetadata2CRSAdapter(null)
    val crs: CoordinateReferenceSystem = try {
      gtcs.createCoordinateSystem(metadata)
    } catch {
      case e: Exception =>
        logWarning("Unable to parse GeoTiff CRS", e)
        null
    }
    val srid: Int = if (crs == null) 0 else CRSServer.crsToSRID(crs, conf)
    rasterMetadata = new RasterMetadata(0, 0, tiffRaster.getWidth, tiffRaster.getHeight,
      tiffRaster.getTileWidth, tiffRaster. getTileHeight, srid, g2m)
  }

  override def metadata: RasterMetadata = rasterMetadata

  override def readTile(tileID: Int): ITile = {
    val tiffTile = if (this.loadTilesLazily) tiffRaster.getLazyTile(tileID) else tiffRaster.getTile(tileID)
    new GeoTiffTile(tileID, tiffTile, fillValue, rasterMetadata)
  }

  /**
   * Whether the given tile contains non-empty data or not. This function does not actually read the tile to
   * determine whether it has valid data or not. Rather, it relies on an optional GeoTiffEntry
   * [[GeoTiffConstants.ValidTilesBitMask]] that explicitly states which tiles are empty or non-empty.
   * @param tileID the ID of the tile to check
   * @return `false` if the tile is known to be empty from the GeoTiff entry or `true` if the tile is not listed
   *         in the entry or if the entry does not exist.
   */
  override def isValidTile(tileID: Int): Boolean = validTiles == null || validTiles.get(tileID)

  override def getTileOffset(tileID: Int): Long = tiffRaster.getTileOffset(tileID)

  override def close(): Unit = tiffReader.close()
}

object GeoTiffReader extends IConfigurable {

  /** Whether to print the output using the pretty printer or not */
  @OperationParam( description = "Load GeoTIFF tiles lazily", showInUsage = false, defaultValue = "false")
  val LazyTiles = "GeoTiffReader.LazyTiles"

}