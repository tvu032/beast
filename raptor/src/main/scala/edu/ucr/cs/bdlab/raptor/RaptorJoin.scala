/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes.RasterRDD

import java.util
import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{IFeature, ITile, RasterMetadata}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.spark.HashPartitioner
import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.locationtech.jts.geom.Geometry

object RaptorJoin {
  /**
   * Performs a raptor join (Raster+Vector) between the rasters in the given path and the given set
   * of geometric features. Each feature is given a numeric ID that is used to produce the output.
   * The return value is a distributed set of results with the following information:
   *  - FeatureID: Long - The ID of the geometry
   *  - RasterID: Int - The index of the file in the given list of files
   *  - x: Int - The x-coordinate of the pixel in the raster file
   *  - y: Int - The y-coordinate of the pixel in the raster file
   *  - m: Float - The value associated with the pixel
   *
   * @param rasters an array of files to process. The index of each entry in this array will be used to associate
   *                the output results to files.
   * @param vector an RDD of pairs of ID and Feature. The ID is used in the output as a reference to the feature.
   * @param opts additional options passed to the raster readers when created
   * @param numTiles an optional accumulator that counts the total number of tiles read
   * @return an RDD of tuples that contain (feature ID, raster ID, x, y, value)
   */
  def raptorJoin(rasters: Array[String], vector: RDD[(Long, IFeature)], opts: BeastOptions,
                 numTiles: LongAccumulator = null) : RDD[RaptorJoinResult] = {
    // Create a Hadoop configuration but load it into BeastOptions because Hadoop Configuration is not serializable
    val hadoopConf = new BeastOptions(opts)
    val sparkConf = vector.sparkContext.getConf
    val iEntry = vector.sparkContext.hadoopConfiguration.iterator()
    while (iEntry.hasNext) {
      val entry = iEntry.next()
      hadoopConf.put(entry.getKey, entry.getValue)
    }
    // intersections represent the following information
    // (Tile ID, (Geometry ID, Y, X1, X2))
    val intersectionsRDD: RDD[(Long, PixelRange)] = vector.mapPartitions { idFeatures: Iterator[(Long, IFeature)] => {
      val conf = hadoopConf.loadIntoHadoopConf(new Configuration(false))
      val intersectionsList = new scala.collection.mutable.ListBuffer[Intersections]()
      val rasterList = new scala.collection.mutable.ListBuffer[Int]()

      val ids = new util.ArrayList[java.lang.Long]()
      val geoms: Array[Geometry] = idFeatures.map(x => {
        ids.add(x._1)
        x._2.getGeometry
      }).toArray

      for (i <- rasters.indices) {
        val raster = rasters(i)
        val rasterPath = new Path(raster)
        val filesystem = rasterPath.getFileSystem(conf)
        val rasterReader: IRasterReader = RasterHelper.createRasterReader(filesystem, rasterPath, opts, sparkConf)
        try {
          val intersections = new Intersections
          intersections.compute(ids, geoms, rasterReader.metadata, opts)
          if (intersections.getNumIntersections != 0) {
            intersectionsList.append(intersections)
            rasterList.append(i)
          }
        } finally {
          rasterReader.close()
        }
      }
      if (intersectionsList.isEmpty)
        Seq[(Long, PixelRange)]().iterator
      else
        new IntersectionsIterator(rasterList.toArray, intersectionsList.toArray)
    }}
    // Same format (Tile ID, (Geometry ID, Y, X1, X2))
    val orderedIntersections: RDD[(Long, PixelRange)] =
      intersectionsRDD.repartitionAndSortWithinPartitions(new HashPartitioner(intersectionsRDD.getNumPartitions))

    // Result is in the format (GeometryID, (RasterFileID, X, Y, RasterValue))
    orderedIntersections.mapPartitions(x =>
      new PixelIterator(x, rasters, opts.getString(IRasterReader.RasterLayerID), numTiles))
  }

  /**
   * Performs a join between the given raster and vector data.
   * The return value is a distributed set of results with the following information:
   *  - FeatureID: Long - The ID of the geometry
   *  - RasterID: Int - The index of the file in the given list of files
   *  - x: Int - The x-coordinate of the pixel in the raster file
   *  - y: Int - The y-coordinate of the pixel in the raster file
   *  - m: Float - The value associated with the pixel
   * @param raster a set of tiles in an RDD[ITile]
   * @param vector a set of (ID, feature) pairs.
   * @param opts additional options for the algorithm
   * @param numTiles an optional accumulator that counts the total number of tiles read
   * @return a set of (Feature ID, pixel) pairs in the form of (FeatureID, RasterFileID, x, y, m)
   */
  def raptorJoin2(raster: RasterRDD, vector: RDD[(Long, IFeature)], opts: BeastOptions,
                  numTiles: LongAccumulator = null) : RDD[RaptorJoinResult] = {
    require(raster.isInstanceOf[RasterFileRDD], "RaptorJoin currently only supports RasterFileRDD")
    val rasterRDD = raster.asInstanceOf[RasterFileRDD]
    val allMetadata: Array[RasterMetadata] = rasterRDD.getRasterMetadata
    val intersectionsRDD = vector.mapPartitions { idFeatures: Iterator[(Long, IFeature)] => {
      val intersectionsList = new scala.collection.mutable.ListBuffer[Intersections]()
      // The index of RasterMetadata that do not have an empty JIT index
      val rasterList = new scala.collection.mutable.ListBuffer[Int]()

      val ids = new util.ArrayList[java.lang.Long]()
      val geoms: Array[Geometry] = idFeatures.map(x => {
        ids.add(x._1)
        x._2.getGeometry
      }).toArray

      for (i <- allMetadata.indices) {
        val rasterMetadata = allMetadata(i)
        val intersections = new Intersections
        intersections.compute(ids, geoms, rasterMetadata, opts)
        if (intersections.getNumIntersections != 0) {
          intersectionsList.append(intersections)
          rasterList.append(i)
        }
      }
      if (intersectionsList.isEmpty)
        Seq[(Long, PixelRange)]().iterator
      else
        new IntersectionsIterator(rasterList.toArray, intersectionsList.toArray)
    }}

    val partitioner = new HashPartitioner(intersectionsRDD.getNumPartitions max rasterRDD.getNumPartitions)
    // Same format (Tile ID, (Geometry ID, Y, X1, X2))
    val orderedIntersections: RDD[(Long, PixelRange)] =
      intersectionsRDD.repartitionAndSortWithinPartitions(partitioner)

    // Partition tiles to match the intersections
    val metadataToID: Map[RasterMetadata, Int] = allMetadata.zipWithIndex.toMap
    val orderedTiles: RDD[(Long, ITile)] = raster.map(tile => {
      assert(metadataToID.contains(tile.rasterMetadata), s"Unexpected! Metadata ${tile.rasterMetadata} " +
        s"not found in ${metadataToID.keys.mkString("\n")}")
      val rasterID = metadataToID(tile.rasterMetadata)
      val rasterTileID = (rasterID.toLong << 32) | tile.tileID
      (rasterTileID, tile)
    }).repartitionAndSortWithinPartitions(partitioner)

    orderedIntersections.zipPartitions(orderedTiles)( (intersections, tiles) => {
      new PixelIterator2(intersections, tiles, numTiles)
    })
  }
}
