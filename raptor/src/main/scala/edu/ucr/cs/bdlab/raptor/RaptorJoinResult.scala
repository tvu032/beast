/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

/**
 * Result of the RaptorJoin operation
 * @param featureID the ID of the geometric (vector) feature
 * @param rasterFileID the file ID of the pixel (raster)
 * @param x the x-coordinate of the pixel (index of the column)
 * @param y the y-coordinate of the pixel (index of the row or scanline)
 * @param m the pixel value from the raster file
 */
case class RaptorJoinResult(featureID: Long, rasterFileID: Int, x: Int, y: Int, m: Float)
