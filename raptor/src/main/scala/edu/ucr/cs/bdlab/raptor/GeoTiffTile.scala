/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.io.tiff.AbstractTiffTile

/**
 * A tile from a GeoTIFF file
 */
class GeoTiffTile(override val tileID: Int, tiffTile: AbstractTiffTile,
                  fillValue: Int, override val rasterMetadata: RasterMetadata) extends ITile {

  override def isEmpty(i: Int, j: Int): Boolean = {
    for (c <- 0 until numComponents) {
      if (tiffTile.getSampleValueAsInt(i, j, c) == fillValue)
        return true
    }
    false
  }

  override def numComponents: Int = tiffTile.getNumSamples

  /**All sample types are float*/
  private lazy val allFloat: Boolean = (0 until numComponents).forall(i => tiffTile.getSampleFormat(i) == 3)

  /**All sample types are int*/
  private lazy val allInt: Boolean = (0 until numComponents).forall(i => tiffTile.getSampleFormat(i) <= 2)

  override def getPixelValue(i: Int, j: Int): Any = {
    if (numComponents == 1) {
      // A single value
      tiffTile.getSampleFormat(0) match {
        case 1 => tiffTile.getSampleValueAsInt(i, j, 0)
        case 2 => tiffTile.getSampleValueAsInt(i, j, 0)
        case 3 => tiffTile.getSampleValueAsFloat(i, j, 0)
      }
    } else if (allFloat) {
      val values = new Array[Float](numComponents)
      for (b <- values.indices)
        values(b) = tiffTile.getSampleValueAsFloat(i, j, b)
      values
    } else if (allInt) {
      val values = new Array[Int](numComponents)
      for (b <- values.indices)
        values(b) = tiffTile.getSampleValueAsInt(i, j, b)
      values
    } else {
      // Array of value
      val values = new Array[AnyVal](numComponents)
      for (b <- values.indices) {
        values(b) = tiffTile.getSampleFormat(b) match {
          case 1 => tiffTile.getSampleValueAsInt(i, j, b)
          case 2 => tiffTile.getSampleValueAsInt(i, j, b)
          case 3 => tiffTile.getSampleValueAsFloat(i, j, b)
        }
      }
      values
    }
  }
}
