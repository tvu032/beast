/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes.{RasterRDD, SpatialRDD}
import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{IFeature, ITile}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

trait RaptorMixin {

  implicit class RasterReadMixinFunctions(sc: SparkContext) {
    /**
     * Loads a GeoTIFF file as an RDD of tiles
     * @param path the path of the file
     * @param iLayer the index of the band to load (0 by default)
     * @param opts additional options for loading the file
     * @return a [[RasterRDD]] that represents all tiles in the file
     */
    def geoTiff(path: String, iLayer: Int = 0, opts: BeastOptions = new BeastOptions): RasterRDD = {
      if (!opts.contains(IRasterReader.RasterLayerID))
        opts.set(IRasterReader.RasterLayerID, iLayer)
      new RasterFileRDD(sc, path, opts)
    }

    def hdfFile(path: String, layer: String, opts: BeastOptions = new BeastOptions()): RasterRDD = {
      opts.set(IRasterReader.RasterLayerID, layer)
      new RasterFileRDD(sc, path, opts)
    }
  }

  implicit class RaptorMixinOperations2(featuresWithIds: RDD[(Long, IFeature)]) {
    def raptorJoin(rasters: Array[String], opts: BeastOptions = new BeastOptions): RDD[RaptorJoinResult] =
      RaptorJoin.raptorJoin(rasters, featuresWithIds, opts)
  }

  implicit class RaptorMixinOperations3(raster: RasterRDD) {
    def raptorJoin(features: SpatialRDD, opts: BeastOptions = new BeastOptions):
      RDD[(IFeature, Int, Int, Int, Float)] = {
      val featuresWithIds: RDD[(Long, IFeature)] = features.zipWithUniqueId().map(x => (x._2, x._1))
      val raptorResults: RDD[RaptorJoinResult] = RaptorJoin.raptorJoin2(raster, featuresWithIds, opts)
      // Join back with the features to produce the desired result
      raptorResults.map(result => (result.featureID, result))
        .join(featuresWithIds)
        .map(x => (x._2._2, x._2._1.rasterFileID, x._2._1.x, x._2._1.y, x._2._1.m))
    }

    /**
     * Save this RasterRDD as a set of geoTIFF files.
     * @param path the path to write the output geotiff files to
     * @param opts additional options for saving the GeoTIFF file
     */
    def saveAsGeoTiff(path: String, opts: BeastOptions = new BeastOptions): Unit =
      GeoTiffWriter.saveAsGeoTiff(raster, path, opts)
  }

  implicit class RaptorMixinOperations(features: SpatialRDD) {
    /**
     * Performs a Raptor join with the given array of raster file.
     * @param rasters a list of raster files to join with
     * @return an RDD with entries in the following format:
     *         - IFeature: One of the input features
     *         - Int: The index of the file from the list of given input files
     *         - Int: The x coordinate of the pixel
     *         - Int: The y coordinate of the pixel
     *         - Float: The measure value from the raster file converted to Float
     */
    def raptorJoin(rasters: Array[String], opts: BeastOptions = new BeastOptions):
      RDD[(IFeature, Int, Int, Int, Float)] = {
      val featuresWithIds: RDD[(Long, IFeature)] = features.zipWithUniqueId().map(x => (x._2, x._1))
      val raptorResults: RDD[RaptorJoinResult] = RaptorJoin.raptorJoin(rasters, featuresWithIds, opts)
      // Join back with the features to produce the desired result
      raptorResults.map(result => (result.featureID, result))
        .join(featuresWithIds)
        .map(x => (x._2._2, x._2._1.rasterFileID, x._2._1.x, x._2._1.y, x._2._1.m))
    }
  }
}

object RaptorMixin extends RaptorMixin
