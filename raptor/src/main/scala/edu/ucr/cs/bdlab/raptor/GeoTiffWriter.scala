/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.io.tiff.{IFDEntry, LZWCodec, TiffConstants}
import edu.ucr.cs.bdlab.beast.util.{BitArray, BitOutputStream, FileUtil, OperationParam}
import org.apache.hadoop.fs.{FSDataOutputStream, FileSystem, Path}
import org.apache.spark.beast.CRSServer
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkContext, TaskContext}
import org.geotools.coverage.grid.io.imageio.geotiff.CRS2GeoTiffMetadataAdapter
import org.opengis.referencing.crs.CoordinateReferenceSystem

import java.io._
import java.util.zip.Deflater
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * A writer for GeoTiff files. It takes [[edu.ucr.cs.bdlab.beast.geolite.RasterMetadata]] at construction
 * and a sequence of [[edu.ucr.cs.bdlab.beast.geolite.ITile]]s to write.
 * The ways this class works is that it writes all tile data to a temporary file. When closed, it will write the
 * GeoTiff header file and appends tile data to it. Therefore, the given file will not be populated with data
 * until the writer is closed.
 * @param outPath the path of the output file that will contain the GeoTIFF after the writer is closed
 * @param rasterMetadata metadata of the raster file that contains CRS and other information
 * @param bare when set to true, the writer will only write tile data without GeoTiff headers to the given output
 * @param opts additional options for the writer
 */
class GeoTiffWriter(val outPath: Path, rasterMetadata: RasterMetadata, bare: Boolean = false,
                    opts: BeastOptions = new BeastOptions())
  extends AutoCloseable {
  import edu.ucr.cs.bdlab.raptor.GeoTiffWriter._

  /**The file system of all output files*/
  private val fileSystem: FileSystem = outPath.getFileSystem(opts.loadIntoHadoopConf())

  // Initialize a temporary file for writing the tile data
  /**A temporary file where all tile data will be written to until the file is closed*/
  private val tileDataPath: Path = if (bare) outPath else new Path(outPath.toString+"_tiledata")

  /**An output stream that writes to the temporary file*/
  private val tileDataOut: FSDataOutputStream = fileSystem.create(tileDataPath)

  /**Keeps track of tile offsets and lengths to write the header at the end*/
  protected val tileOffsetsLengths = new Array[(Long, Long)](rasterMetadata.numTiles)

  /**Number of bits for each sample*/
  private val bitsPerSample: Array[Int] = opts.getString(BitsPerSample, "8").split(",").map(_.toInt)

  /**Data types of all samples*/
  private val sampleFormats: Array[Int] = opts.getString(SampleFormats, "1").split(",").map(_.toInt)

  /**The value that marks invalid pixels*/
  private var fillValue: Int = 0

  def setFillValue(value: Int): Unit = this.fillValue = value

  /**Type of compression for tiles*/
  val compression: Int = opts.getInt(Compression, TiffConstants.COMPRESSION_LZW)

  /**The predictor determines whether to apply difference to */
  val predictor: Int = if (compression == TiffConstants.COMPRESSION_LZW)
    opts.getInt(Predictor, if (compression == TiffConstants.COMPRESSION_LZW) 1 else 0)
  else 0

  /**Number of bands in the file*/
  var numBands: Int = 1

  /**
   * Write the given tile to the raster file
   * @param tile the tile to write to the file
   */
  def write(tile: ITile): Unit = {
    assert(tileOffsetsLengths(tile.tileID) == null, s"Duplicate tile ${tile.tileID}")
    numBands = tile.numComponents
    val tileOffset: Long = tileDataOut.getPos

    // Since a tile is typically small, we first write the entire tile to memory and then compress if needed.
    val baos = new ByteArrayOutputStream()
    val pixelOutput = new BitOutputStream(baos)
    for (j <- tile.y1 to tile.y2; i <- tile.x1 to tile.x2) {
      // TODO apply the difference predictor
      if (tile.isEmpty(i, j)) {
        // Empty pixel. Write the fill value for all value
        for (b <- 0 until numBands)
          pixelOutput.write(fillValue, bitsPerSample(b))
      } else {
        // A valid value
        if (numBands == 1) {
          tile.getPixelValue(i, j) match {
            case i: Int => pixelOutput.write(i, bitsPerSample(0))
            case f: Float => pixelOutput.write(java.lang.Float.floatToIntBits(f), bitsPerSample(0))
          }
        } else {
          tile.getPixelValue(i, j) match {
            case is: Array[Int] =>
              for (b <- 0 until numBands)
                pixelOutput.write(is(b), bitsPerSample(b))
            case fs: Array[Float] =>
              for (b <- 0 until numBands)
                pixelOutput.write(java.lang.Float.floatToIntBits(fs(b)), bitsPerSample(b))
            case os: Array[Any] =>
              for (b <- 0 until numBands) {
                os(b) match {
                  case i: Int => pixelOutput.write(i, bitsPerSample(b))
                  case f: Float => pixelOutput.write(java.lang.Float.floatToIntBits(f), bitsPerSample(b))
                }
              }
          }
        }
      }
    }
    pixelOutput.close()
    val decompressedData = baos.toByteArray
    // Use the configured compression
    compression match {
      case TiffConstants.COMPRESSION_NONE =>
        tileDataOut.write(decompressedData)
      case TiffConstants.COMPRESSION_DEFLATE =>
        val compressor = new Deflater()
        compressor.setInput(decompressedData)
        compressor.finish()
        val compressedData = new Array[Byte](16384)
        while (!compressor.finished()) {
          val compressedSize = compressor.deflate(compressedData)
          tileDataOut.write(compressedData, 0, compressedSize)
        }
      case TiffConstants.COMPRESSION_LZW =>
        // Write as a byte array and then compress using LZW if needed
        tileDataOut.write(LZWCodec.encode(decompressedData))
    }
    val tileLength: Long = tileDataOut.getPos - tileOffset
    tileOffsetsLengths(tile.tileID) = (tileOffset, tileLength)
  }

  /**
   * Close the tile data output without writing a header file. Returns an array of offsets and lengths of all
   * written tiles.
   * @return an array equal in length to number of tiles. Each entry contains a pair of (offset, length) for
   *         the corresponding tile IDs. Tiles that were not written will have null.
   */
  def closeTileDataOnly(): Array[(Long, Long)] = {
    tileDataOut.close()
    fileSystem.rename(tileDataPath, outPath)
    tileOffsetsLengths
  }

  override def close(): Unit = {
    if (bare) {
      // A bare file
      tileDataOut.close()
      return
    }
    // Full mode
    val nonWrittenTiles: Array[Int] = tileOffsetsLengths.zipWithIndex.filter(_._1 == null).map(_._2)
    if (nonWrittenTiles.nonEmpty) {
      // Create an additional empty tile and assign its location to all non-written tiles
      val emptyTile: ITile = new EmptyTile(nonWrittenTiles.head, rasterMetadata, numBands)
      write(emptyTile)
      // Assign the same offset and length to all remaining tiles
      for (nonWrittenTileID <- nonWrittenTiles)
        tileOffsetsLengths(nonWrittenTileID) = tileOffsetsLengths(nonWrittenTiles(0))
    }
    // Now, it is time to write the actual file
    tileDataOut.close()

    // Prepend the header
    prependGeoTiffHeader(rasterMetadata, bitsPerSample, sampleFormats, compression, tileOffsetsLengths, fillValue,
      nonWrittenTiles, outPath, Array(tileDataPath), opts)
  }
}

object GeoTiffWriter {
  @OperationParam(description = "Overwrite the output if it already exists {true, false}.", defaultValue = "false")
  val OverwriteOutput = "overwrite"

  @OperationParam(description = "Bits per sample. Comma separated list", defaultValue = "8")
  val BitsPerSample = "bitspersample"

  @OperationParam(description = "The data type of all samples (1=Unsigned Int, 2= Signed Int, 3=Float)", defaultValue = "1")
  val SampleFormats = "sampleformats"

  @OperationParam(description = "Compression technique to use for GeoTIFF (1=PackBits, 5=LZW, 8=Deflate)", defaultValue = "5")
  val Compression = "compression"

  @OperationParam(description = "GeoTIFF predictor (0=none, 1=difference)", defaultValue = "0")
  val Predictor = "predictor"

  @OperationParam(description = "The mode for writing the file {compatibility, distributed}", defaultValue = "distributed")
  val WriteMode = "writemode"

  // TODO add other options such as writing Big GeoTIFF or big/little endian

  /**
   * Create a temporary IFDEntry with one of the standard data types
   * @param tag
   * @param valueType
   * @param value
   * @return
   */
  def makeIFDEntry(tag: Short, valueType: Short, value: Any): TempIFDEntry = {
    val finalValue = value match {
      case s: String => s
      case v: Array[Int] => v
      case v: Array[Double] => v
      case v: Byte => Array[Int](v.toInt)
      case v: Short => Array[Int](v.toInt)
      case v: Int => Array[Int](v)
      case v: Long => Array[Int](v.toInt)
      case v: Double => Array[Double](v)
      case v: Array[Byte] => v.map(_.toInt)
      case v: Array[Short] => v.map(_.toInt)
      case v: Array[Long] => v.map(_.toInt)
    }
    TempIFDEntry(tag, valueType, finalValue)
  }

  /**
   * Save a RasterRDD as a GeoTiff file in compatibility mode where it produces a set of GeoTiff files that can be
   * read by other GIS software such as QGIS. It concatenates all the tiles that belong to the same RasterMetadata
   * (same file) into one file that contains all tiles for all partitions.
   * @param rasterRDD the RDD of tiles to write to the output
   * @param outPath the output path to write the file to
   * @param opts additional options for writing the file
   */
  private def saveAsGeoTiffCompatibilityMode(rasterRDD: RDD[ITile], outPath: String, opts: BeastOptions): Unit = {
    val sc: SparkContext = rasterRDD.sparkContext
    val tempPath: String = new File(outPath, "temp").getPath
    val interimResults: Array[(RasterMetadata, String, Array[(Long, Long)])] =
      sc.runJob(rasterRDD, (taskContext: TaskContext, tiles: Iterator[ITile]) => {
      val taskTempDir: Path = new Path(new Path(tempPath), taskContext.taskAttemptId().toString)
      val fileSystem: FileSystem = taskTempDir.getFileSystem(opts.loadIntoHadoopConf())
      fileSystem.mkdirs(taskTempDir)
      // Cleanup handler
      val deleteTempDir: (TaskContext, Throwable) => Unit = (_, _) => fileSystem.delete(taskTempDir, true)
      taskContext.addTaskFailureListener(deleteTempDir)
      val writers: mutable.HashMap[RasterMetadata, GeoTiffWriter] = new mutable.HashMap()
      // Completion handler that closes all open files
      val closeAllFiles: TaskContext => Unit = _ => for (writer <- writers.valuesIterator) writer.close()
      taskContext.addTaskCompletionListener[Unit](closeAllFiles)
      for (tile <- tiles) {
        val writer = writers.getOrElseUpdate(tile.rasterMetadata, {
          val filename = f"part-${taskContext.partitionId()}%05d-${writers.size}%03d_tiledata"
          val geoTiffPath = new Path(taskTempDir, filename)
          val newWriter = new GeoTiffWriter(geoTiffPath, tile.rasterMetadata, true, opts)
          newWriter
        })
        writer.write(tile)
      }
      writers.map(w => {
        (w._1, w._2.outPath.toString, w._2.tileOffsetsLengths)
      }).toArray
    }).flatMap(_.iterator)
    // Merge intermediate files for each RasterMetadata
    // Build a table of intermediate data for each file.
    // Each file is identified by RasterMetadata
    val resultsPerFile: mutable.HashMap[RasterMetadata, (Array[String], Array[(Long, Long)])] = new mutable.HashMap()
    for (interimResult <- interimResults) {
      val existingResult = resultsPerFile.get(interimResult._1)
      if (existingResult.isEmpty) {
        // First encounter of the metadata. Create a new entry
        resultsPerFile.put(interimResult._1, (Array(interimResult._2), interimResult._3))
      } else {
        // Merge with existing result
        val result = existingResult.get
        val listOfFiles = result._1 :+ interimResult._2
        val tileOffsetsLengths = result._2
        val totalSize = tileOffsetsLengths.map(x => if (x != null) x._1 + x._2 else 0).max
        for (tileID <- interimResult._3.indices; if interimResult._3(tileID) != null) {
          require(tileOffsetsLengths(tileID) == null, s"Duplicate tile ${tileID}")
          tileOffsetsLengths(tileID) = (interimResult._3(tileID)._1 + totalSize, interimResult._3(tileID)._2)
        }
        resultsPerFile.put(interimResult._1, (listOfFiles, tileOffsetsLengths))
      }
    }
    // Now, merge all partial files for each RasterMetadata
    var fileIndex: Int = 0
    for ((metadata, (_tileDataPaths, tileOffsetsLengths)) <- resultsPerFile) {
      val bitsPerSample: Array[Int] = opts.getString(BitsPerSample, "8").split(",").map(_.toInt)
      val sampleFormats: Array[Int] = opts.getString(SampleFormats, "1").split(",").map(_.toInt)
      val compression: Int = opts.getInt(Compression, TiffConstants.COMPRESSION_LZW)
      val fillValue: Int = 0

      val emptyTiles: Array[Int] = tileOffsetsLengths.zipWithIndex.filter(_._1 == null).map(_._2)
      var tileDataPaths: Array[String] = _tileDataPaths
      if (emptyTiles.nonEmpty) {
        // Create a new file that contains an empty tile
        val emptyFile = new Path(new Path(tileDataPaths.head).getParent, "emptytile")
        val emptyWriter = new GeoTiffWriter(emptyFile, metadata, true, opts)
        val emptyTile: ITile = new EmptyTile(emptyTiles.head, metadata, bitsPerSample.length)
        emptyWriter.write(emptyTile)
        emptyWriter.close()
        val emptyTileLength = emptyWriter.tileOffsetsLengths(emptyTile.tileID)._2
        val emptyTileOffset = tileOffsetsLengths.map(o => if (o != null) o._1 + o._2 else 0).max
        for (emptyTileID <- emptyTiles)
          tileOffsetsLengths(emptyTileID) = (emptyTileOffset, emptyTileLength)
        tileDataPaths = tileDataPaths :+ emptyFile.toString
      }

      val filename = f"part-${fileIndex}%05d.tif"
      val geoTiffPath: Path = new Path(new Path(outPath), filename)
      prependGeoTiffHeader(metadata, bitsPerSample, sampleFormats, compression, tileOffsetsLengths,
        fillValue, emptyTiles, geoTiffPath, tileDataPaths.map(new Path(_)), opts)
      fileIndex += 1
    }
  }

  /**
   * Writes a raster RDD as a set of GeoTiff files.
   * @param rasterRDD the RDD of tiles to write to the output
   * @param outPath the output path to write the file to
   * @param opts additional options for writing the file
   */
  private def saveAsGeoTiffDistributedMode(rasterRDD: RDD[ITile], outPath: String, opts: BeastOptions): Unit = {
    val sc: SparkContext = rasterRDD.sparkContext
    val tempPath: String = new File(outPath, "temp").getPath
    val interimFiles: Array[String] = sc.runJob(rasterRDD, (taskContext: TaskContext, tiles: Iterator[ITile]) => {
      val taskTempDir: Path = new Path(new Path(tempPath), taskContext.taskAttemptId().toString)
      val fileSystem: FileSystem = taskTempDir.getFileSystem(opts.loadIntoHadoopConf())
      fileSystem.mkdirs(taskTempDir)
      // Cleanup handler
      val deleteTempDir: (TaskContext, Throwable) => Unit = (_, _) => fileSystem.delete(taskTempDir, true)
      taskContext.addTaskFailureListener(deleteTempDir)
      val writers: mutable.HashMap[RasterMetadata, GeoTiffWriter] = new mutable.HashMap()
      // Completion handler that closes all open files
      val closeAllFiles: TaskContext => Unit = _ => for (writer <- writers.valuesIterator) writer.close()
      taskContext.addTaskCompletionListener[Unit](closeAllFiles)
      for (tile <- tiles) {
        val writer = writers.getOrElseUpdate(tile.rasterMetadata, {
          val filename = f"part-${taskContext.partitionId()}%05d-${writers.size}%03d.tif"
          val geoTiffPath = new Path(taskTempDir, filename)
          val newWriter = new GeoTiffWriter(geoTiffPath, tile.rasterMetadata, false, opts)
          newWriter
        })
        writer.write(tile)
      }
      taskTempDir.toString
    })
    // Move interim files to the output directory and cleanup any remaining files
    val outP: Path = new Path(outPath)
    val fileSystem = outP.getFileSystem(opts.loadIntoHadoopConf())
    for (interimFile <- interimFiles) {
      val files = fileSystem.listStatus(new Path(interimFile))
      for (file <- files) {
        val targetFile = new Path(outP, file.getPath.getName)
        fileSystem.rename(file.getPath, targetFile)
      }
      fileSystem.delete(new Path(interimFile), false)
    }
    fileSystem.delete(new Path(tempPath), false)
  }

  def saveAsGeoTiff(rasterRDD: RDD[ITile], outPath: String, opts: BeastOptions): Unit = {
    opts.getString(WriteMode, "distributed") match {
      case "distributed" => saveAsGeoTiffDistributedMode(rasterRDD, outPath, opts)
      case "compatibility" => saveAsGeoTiffCompatibilityMode(rasterRDD, outPath, opts)
    }
  }

  /**
   * Takes a file that contains tile data and prepends an appropriate GeoTiff header to it to make it a correct
   * GeoTiff file.
   * @param rasterMetadata the metadata of the raster file
   * @param bitsPerSample number of bits for each sample in the tile [[BitsPerSample]]
   * @param sampleFormats the formats of each sample in the file [1, 2 = Int, 3 = Float) [[SampleFormats]]
   * @param compression the compression technique used to write tile data [[Compression]]
   * @param tileOffsetsLengths an array of length rasterMetadata.numTiles that tells the offset and length of each
   *                           tile in the concatenated tile data files not accounting for the header
   * @param fillValue the value that marks empty pixels or `null` if no such value
   * @param emptyTiles the list of tile IDs that are completely empty or `null` if unknown
   * @param outPath the path to write the final GeoTiff file
   * @param tileDataPath the list of files (in order) that contain tile data
   * @param opts additional user-defined options
   */
  def prependGeoTiffHeader(rasterMetadata: RasterMetadata, bitsPerSample: Array[Int],
                           sampleFormats: Array[Int], compression: Int, tileOffsetsLengths: Array[(Long, Long)],
                           fillValue: Int, emptyTiles: Array[Int], outPath: Path, tileDataPath: Array[Path],
                           opts: BeastOptions): Unit = {
    val numBands = bitsPerSample.length
    val software: String = "UCR Beast"
    // 1. Prepare the list of entries
    var ifdEntries: mutable.ArrayBuffer[TempIFDEntry] = new ArrayBuffer[TempIFDEntry]()
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_SOFTWARE, TiffConstants.TYPE_ASCII, software))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_IMAGE_WIDTH, TiffConstants.TYPE_LONG, rasterMetadata.rasterWidth))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_IMAGE_LENGTH, TiffConstants.TYPE_LONG, rasterMetadata.rasterHeight))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_SAMPLES_PER_PIXEL, TiffConstants.TYPE_SHORT, numBands))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_BITS_PER_SAMPLE, TiffConstants.TYPE_SHORT, bitsPerSample))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_SAMPLE_FORMAT, TiffConstants.TYPE_SHORT, Array.fill(numBands)(sampleFormats(0))))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_COMPRESSION, TiffConstants.TYPE_SHORT, compression))
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_PHOTOMETRIC_INTERPRETATION, TiffConstants.TYPE_SHORT, 2))
    // If the file has only one strip or tile, the offset and length should be encoded in the IFD Entry
    if (rasterMetadata.tileWidth == rasterMetadata.rasterWidth) {
      // Stripped file
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_STRIP_OFFSETS, TiffConstants.TYPE_LONG, tileOffsetsLengths.map(_._1)))
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_STRIP_BYTE_COUNTS, TiffConstants.TYPE_LONG, tileOffsetsLengths.map(_._2)))
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_ROWS_PER_STRIP, TiffConstants.TYPE_LONG, rasterMetadata.tileHeight))
    } else {
      // Tiled file
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_TILE_OFFSETS, TiffConstants.TYPE_LONG, tileOffsetsLengths.map(_._1)))
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_TILE_BYTE_COUNTS, TiffConstants.TYPE_LONG, tileOffsetsLengths.map(_._2)))
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_TILE_WIDTH, TiffConstants.TYPE_LONG, rasterMetadata.tileWidth))
      ifdEntries.append(makeIFDEntry(TiffConstants.TAG_TILE_LENGTH, TiffConstants.TYPE_LONG, rasterMetadata.tileHeight))
    }
    if (emptyTiles.nonEmpty) {
      // Add a tag with a bit-mask for non-empty tiles
      val validTiles = new BitArray(rasterMetadata.numTiles)
      validTiles.setRange(0, rasterMetadata.numTiles, true)
      for (emptyTileId <- emptyTiles)
        validTiles.set(emptyTileId, false)
      val baos = new ByteArrayOutputStream()
      val dos = new DataOutputStream(baos)
      validTiles.writeBitsMinimal(dos)
      dos.close()
      ifdEntries.append(makeIFDEntry(GeoTiffConstants.ValidTilesBitMask, TiffConstants.TYPE_BYTE,
        baos.toByteArray.map(_.toInt)))
    }
    ifdEntries.append(makeIFDEntry(TiffConstants.TAG_PLANAR_CONFIGURATION, TiffConstants.TYPE_SHORT, 1))
    // Fill value
    ifdEntries.append(makeIFDEntry(GeoTiffConstants.GDALNoDataTag, TiffConstants.TYPE_ASCII, fillValue.toString))
    if (rasterMetadata.srid != 0) {
      val crs: CoordinateReferenceSystem = CRSServer.sridToCRS(rasterMetadata.srid, opts.loadIntoSparkConf())
      val adapter = new CRS2GeoTiffMetadataAdapter(crs)
      val data = adapter.parseCoordinateReferenceSystem()
      val entryData = new Array[Int](data.getNumGeoKeyEntries * 4)
      var doubleParams: Array[Double] = Array()
      var asciiParams: String = ""
      entryData(0) = data.getGeoTIFFVersion
      entryData(1) = data.getKeyRevisionMajor
      entryData(2) = data.getKeyRevisionMinor
      entryData(3) = data.getNumGeoKeyEntries - 1
      for (i <- 1 until data.getNumGeoKeyEntries) {
        val geoKeyEntry = data.getGeoKeyEntryAt(i)
        entryData(i * 4 + 0) = geoKeyEntry.getKeyID
        entryData(i * 4 + 1) = geoKeyEntry.getTiffTagLocation
        entryData(i * 4 + 2) = geoKeyEntry.getCount
        entryData(i * 4 + 3) = geoKeyEntry.getValueOffset
        if (geoKeyEntry.getTiffTagLocation.toShort == GeoTiffConstants.GeoDoubleParamsTag) {
          entryData(i * 4 + 3) = doubleParams.length
          doubleParams = doubleParams ++ data.getGeoDoubleParams(geoKeyEntry.getKeyID)
        }
        if (geoKeyEntry.getTiffTagLocation.toShort == GeoTiffConstants.GeoAsciiParamsTag) {
          entryData(i * 4 + 3) = asciiParams.length
          asciiParams = asciiParams + data.getGeoAsciiParam(geoKeyEntry.getKeyID)
        }
      }
      ifdEntries.append(makeIFDEntry(GeoTiffConstants.GeoKeyDirectoryTag, TiffConstants.TYPE_SHORT, entryData))
      if (doubleParams.nonEmpty)
        ifdEntries.append(makeIFDEntry(GeoTiffConstants.GeoDoubleParamsTag, TiffConstants.TYPE_DOUBLE, doubleParams))
      if (asciiParams.nonEmpty)
        ifdEntries.append(makeIFDEntry(GeoTiffConstants.GeoAsciiParamsTag, TiffConstants.TYPE_ASCII, asciiParams))
    }
    // Write Affine Transform
    if (rasterMetadata.g2m != null) {
      val affineTransform = rasterMetadata.g2m
      if (affineTransform.getShearX == 0.0 & affineTransform.getShearY == 0.0) {
        // The most common special case when no shearing is in the transformation matrix
        ifdEntries.append(makeIFDEntry(GeoTiffConstants.ModelTiepointTag, TiffConstants.TYPE_DOUBLE,
          Array(0.0, 0.0, 0.0, affineTransform.getTranslateX, affineTransform.getTranslateY, 0.0)))
        ifdEntries.append(makeIFDEntry(GeoTiffConstants.ModelPixelScaleTag, TiffConstants.TYPE_DOUBLE,
          Array(affineTransform.getScaleX, -affineTransform.getScaleY, 0.0)))
      } else {
        // TODO A full affine transformation, write all entries
        throw new RuntimeException("Full affine transformations not yet supported")
      }
    }
    // Sort by tag as required by TIFF specifications
    ifdEntries = ifdEntries.sortBy(_.tag & 0xffff)

    // 2. Now, estimate the size of the header to assign offsets to IFD entries and adjust tile offsets in file
    var metadataSize: Int = 0
    metadataSize = 8 // 2 byte ordering + Identifier (42) + offset of first IFD entry
    metadataSize += 2 + ifdEntries.length * 12 + 4 // IFD Table
    for (entry <- ifdEntries) {
      val valueLength = entry.valueLength
      if (valueLength > 4) {
        entry.offset = metadataSize
        metadataSize += valueLength
      }
    }
    // Update tile offsets based on the metadata size since tiles will be written after the header + metadata
    val offsetsArray: Array[Int] = ifdEntries
      .find(e => e.tag == TiffConstants.TAG_TILE_OFFSETS || e.tag == TiffConstants.TAG_STRIP_OFFSETS)
      .get.value.asInstanceOf[Array[Int]]
    for (tileID <- offsetsArray.indices)
      offsetsArray(tileID) = offsetsArray(tileID) + metadataSize

    val fileSystem: FileSystem = outPath.getFileSystem(opts.loadIntoHadoopConf())
    val headerPath = new Path(outPath.toString + "_header")
    // 3. Write the actual file to the given output stream
    val headerOut = fileSystem.create(headerPath)
    headerOut.writeShort(TiffConstants.BIG_ENDIAN)
    headerOut.writeShort(TiffConstants.SIGNATURE)
    headerOut.writeInt(8)
    // Write IFD Table
    headerOut.writeShort(ifdEntries.length)
    for (entry <- ifdEntries) {
      headerOut.writeShort(entry.tag)
      headerOut.writeShort(entry.valueType)
      headerOut.writeInt(entry.count)
      headerOut.writeInt(entry.getValueOrOffset())
    }
    headerOut.writeInt(0)
    // Write long values that do not fit in the IFDEntry itself
    for (entry <- ifdEntries) {
      if (entry.valueLength > 4) {
        assert(entry.offset != 0, "Unexpected zero offset for a big value")
        entry.valueType match {
          case TiffConstants.TYPE_ASCII =>
            headerOut.writeBytes(entry.value.asInstanceOf[String])
            headerOut.writeByte(0)
          case TiffConstants.TYPE_BYTE =>
            for (v <- entry.value.asInstanceOf[Array[Int]])
              headerOut.writeByte(v)
          case TiffConstants.TYPE_SHORT =>
            for (v <- entry.value.asInstanceOf[Array[Int]])
              headerOut.writeShort(v)
          case TiffConstants.TYPE_LONG =>
            for (v <- entry.value.asInstanceOf[Array[Int]])
              headerOut.writeInt(v)
          case TiffConstants.TYPE_DOUBLE =>
            for (v <- entry.value.asInstanceOf[Array[Double]])
              headerOut.writeDouble(v)
        }
      }
    }
    headerOut.close()

    // Concatenate both files
    FileUtil.concat(fileSystem, outPath, (headerPath +: tileDataPath):_*)
  }
}

/**
 * A temporary IFDEntry that is kept in memory before being written to disk
 * @param tag the numeric tag
 * @param valueType the type of the value stored in this entry
 * @param value the values stored in this entry. The value must be one of two possibilities,
 *              Array[Int] for all numeric values, Array[Double] for doubles, or String
 */
case class TempIFDEntry(tag: Short, valueType: Short, value: Any, var offset: Int = 0) {
  require(value.isInstanceOf[Array[Int]] || value.isInstanceOf[String] || value.isInstanceOf[Array[Double]])

  def count: Int = value match {
    case ar: Array[Int] => ar.length
    case ar: Array[Double] => ar.length
    case str: String => str.length + 1
  }

  /**Total length of the value in bytes*/
  def valueLength: Int = TiffConstants.TypeSizes(this.valueType) * count

  /**The value or offset that should be written to the file*/
  def getValueOrOffset(bigEndian: Boolean = true): Int = {
    if (valueLength > 4) {
      offset
    } else value match {
      case ar: Array[Int] => IFDEntry.makeValue(valueType, ar, bigEndian)
      case str: String => IFDEntry.makeValue(valueType, (str.getBytes.map(_.toInt) :+ 0), bigEndian)
    }
  }

  override def toString: String = {
    val valueString: String = value match {
      case s: String => s"'${s}'"
      case v: Array[Int] => s"[${v.mkString(",")}]"
      case ar: Array[Double] => s"[${ar.mkString(",")}]"
    }
    val tagName: String = TiffConstants.TagNames.get(tag)
    s"IFDEntry #${tag} ${tagName} value ${valueString} offset ${offset}"
  }
}